/* Compile with:
 * wayland-scanner private-code /usr/share/wayland-protocols/stable/xdg-shell/xdg-shell.xml xdg-shell.c
 * wayland-scanner client-header /usr/share/wayland-protocols/stable/xdg-shell/xdg-shell.xml xdg-shell.h
 * gcc -c xdg-shell.c
 * gcc -c egl-on-wayland-xdg.c
 * gcc -o egl-on-wayland-xdg xdg-shell.o egl-on-wayland-xdg.o -lwayland-egl -lwayland-client -lEGL -lGL
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <wayland-client-core.h>
#include <wayland-egl.h>
#include <EGL/egl.h>
#include <GL/gl.h>

#include "xdg-shell.h"

struct client_state {
	struct wl_display *display;
	struct wl_registry *registry;
	struct wl_compositor *compositor;
	struct xdg_wm_base *xdg_wm_base;

	struct wl_surface *surface;
	struct xdg_surface *xdg_surface;
	struct xdg_toplevel *xdg_toplevel;
	struct wl_egl_window *egl_window;

	EGLDisplay egl_display;
	EGLConfig egl_config;
	EGLContext egl_context;
	EGLSurface egl_surface;

	int32_t width;
	int32_t height;
	uint8_t running;
};

/******************************/
/***********Registry***********/
/******************************/

static void global_registry(void *data, struct wl_registry *wl_registry,
		    	uint32_t name, const char *interface, uint32_t version) {
	struct client_state *state = data;

	if(!strcmp(interface, wl_compositor_interface.name)) {
		state->compositor = wl_registry_bind(wl_registry, name,
								&wl_compositor_interface, 1);
	} else if(!strcmp(interface, xdg_wm_base_interface.name)) {
		state->xdg_wm_base = wl_registry_bind(wl_registry, name,
								&xdg_wm_base_interface, 1);
	}
}

static void global_remove(void *data, struct wl_registry *wl_registry,
			    uint32_t name) {
	(void) data;
	(void) wl_registry;
	(void) name;
}

static const struct wl_registry_listener registry_listener = {
	global_registry,
	global_remove
};

/******************************/
/******XDG Window Manager******/
/******************************/

static void wm_ping(void *data, struct xdg_wm_base *xdg_wm_base,
				uint32_t serial) {
	(void) data;
	xdg_wm_base_pong(xdg_wm_base, serial);
}

static const struct xdg_wm_base_listener wm_base_listener = {
	wm_ping
};

/******************************/
/*********XDG Surface**********/
/******************************/

static void surface_configure(void *data, struct xdg_surface *xdg_surface,
				uint32_t serial) {
	(void) data;

	xdg_surface_ack_configure(xdg_surface, serial);
}

static const struct xdg_surface_listener surface_listener = {
	surface_configure
};

/******************************/
/********XDG Toplevel**********/
/******************************/

static void toplevel_configure(void *data, struct xdg_toplevel *xdg_toplevel,
				int32_t width, int32_t height, struct wl_array *states) {
	struct client_state *state = data;
	(void) xdg_toplevel;
	(void) states;

	if(!width && !height) return;

	if(state->width != width || state->height != height) {
		state->width = width;
		state->height = height;

		wl_egl_window_resize(state->egl_window, width, height, 0, 0);
		wl_surface_commit(state->surface);
	}
}

static void toplevel_close(void *data, struct xdg_toplevel *xdg_toplevel) {
	(void) xdg_toplevel;

	struct client_state *state = data;

	state->running = 0;
}

static const struct xdg_toplevel_listener toplevel_listener = {
	toplevel_configure,
	toplevel_close
};

/******************************/
/******************************/
/******************************/

static void wayland_connect(struct client_state *state) {
	state->display = wl_display_connect(NULL);
	if(!state->display) {
		fprintf(stderr, "Couldn't connect to wayland display\n");
		exit(EXIT_FAILURE);
	}

	state->registry = wl_display_get_registry(state->display);
	wl_registry_add_listener(state->registry, &registry_listener, state);
	wl_display_roundtrip(state->display);
	if(!state->compositor || !state->xdg_wm_base) {
		fprintf(stderr, "Couldn't find compositor or xdg shell\n");
		exit(EXIT_FAILURE);
	}

	xdg_wm_base_add_listener(state->xdg_wm_base, &wm_base_listener, NULL);

	state->surface = wl_compositor_create_surface(state->compositor);
	state->xdg_surface = xdg_wm_base_get_xdg_surface(state->xdg_wm_base,
							state->surface);
	xdg_surface_add_listener(state->xdg_surface, &surface_listener, NULL);
	state->xdg_toplevel = xdg_surface_get_toplevel(state->xdg_surface);
	xdg_toplevel_set_title(state->xdg_toplevel, "Hello World");
	xdg_toplevel_add_listener(state->xdg_toplevel, &toplevel_listener, state);
	wl_surface_commit(state->surface);
	wl_display_roundtrip(state->display);
}

static void egl_init(struct client_state *state) {
	EGLint major;
	EGLint minor;
	EGLint num_configs;
	EGLint attribs[] = {
		EGL_RED_SIZE, 8,
		EGL_GREEN_SIZE, 8,
		EGL_BLUE_SIZE, 8,
	EGL_NONE};

	eglBindAPI (EGL_OPENGL_API);
	state->egl_window = wl_egl_window_create(state->surface, state->width,
							state->height);

	state->egl_display = eglGetDisplay((EGLNativeDisplayType) state->display);
	if(state->display == EGL_NO_DISPLAY) {
		fprintf(stderr, "Couldn't get EGL display\n");
		exit(EXIT_FAILURE);
	}

	if(eglInitialize(state->egl_display, &major, &minor) != EGL_TRUE) {
		fprintf(stderr, "Couldn't initialize EGL\n");
		exit(EXIT_FAILURE);
	}

	if(eglChooseConfig(state->egl_display, attribs, &state->egl_config, 1,
			&num_configs) != EGL_TRUE) {
		fprintf(stderr, "Couldn't find matching EGL config\n");
		exit(EXIT_FAILURE);
	}

	state->egl_surface = eglCreateWindowSurface(state->egl_display,
							state->egl_config,
							(EGLNativeWindowType) state->egl_window, NULL);
	if(state->egl_surface == EGL_NO_SURFACE) {
		fprintf(stderr, "Couldn't create EGL surface\n");
		exit(EXIT_FAILURE);
	}

	state->egl_context = eglCreateContext(state->egl_display, state->egl_config,
							EGL_NO_CONTEXT, NULL);
	if(state->egl_context == EGL_NO_CONTEXT) {
		fprintf(stderr, "Couldn't create EGL context\n");
		exit(EXIT_FAILURE);
	}

	if(!eglMakeCurrent(state->egl_display, state->egl_surface,
			state->egl_surface, state->egl_context)) {
		fprintf(stderr, "Couldn't make EGL context current\n");
		exit(EXIT_FAILURE);
	}
}

static void draw(struct client_state *state) {
	static int frame = 0;
	frame ++;
	int blue_amount = frame % 255;


	glClearColor(0.0/255.0, 79.0/255.0, blue_amount/255.0, 1.0);
	glClear(GL_COLOR_BUFFER_BIT);

	if (frame % 255 == 0) {
		printf("resizing\n");
		if (state->width == 800) {
			state->width = 400;
		} else {
			state->width = 800;
		}
		wl_egl_window_resize(state->egl_window, state->width, state->height, 0, 0);
	}

	eglSwapBuffers(state->egl_display, state->egl_surface);
}

int main(int argc, char const *argv[]) {
	struct client_state state;

	state.width = 800;
	state.height = 600;
	state.running = 1;

	wayland_connect(&state);

	egl_init(&state);
	eglSwapInterval(state.egl_display, 0);


	while(state.running) {
		wl_display_dispatch_pending(state.display);
		usleep(5000);
		draw(&state);
	}

	eglDestroySurface(state.egl_display, state.egl_surface);
	eglDestroyContext(state.egl_display, state.egl_context);
	wl_egl_window_destroy(state.egl_window);
	xdg_toplevel_destroy(state.xdg_toplevel);
	xdg_surface_destroy(state.xdg_surface);
	wl_surface_destroy(state.surface);
	wl_display_disconnect(state.display);

	return 0;
}
